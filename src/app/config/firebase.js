import firebase from 'firebase';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyDrK7ivEmRVVFvm28YxhV6gp3TnDSpV8Bo",
    authDomain: "revents-217418.firebaseapp.com",
    databaseURL: "https://revents-217418.firebaseio.com",
    projectId: "revents-217418",
    storageBucket: "revents-217418.appspot.com",
    messagingSenderId: "351413046942"
}
//date objesi ile hata verdigi icin
firebase.initializeApp(firebaseConfig);
const firestore = firebase.firestore();
const settings = {
    timestampsInSnapshots: true
}
firestore.settings(settings);

export default firebase;