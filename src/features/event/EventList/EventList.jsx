import React, { Component } from 'react'
import EventListItem from './EventListItem';

class EventList extends Component {
  
  render() {

    const {events, deleteEvent} = this.props;

    return (
      <div>
        {/*cift and isareti islemi bir nevi async yapti (aslinda degil tabi) sanki firebase e baglanırken */}
        {events && events.map((event) => (
          <EventListItem key={event.id} event={event} deleteEvent={deleteEvent} />
        ))}

        
       
      </div>
    )
  }
}

export default EventList;