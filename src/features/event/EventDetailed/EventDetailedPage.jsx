import React, { Component } from 'react';
import { withFirestore } from 'react-redux-firebase';
import EventDetailedHeader from './EventDetailedHeader';
import EventDetailedSideBar from './EventDetailedSideBar';
import EventDetailedChat from './EventDetailedChat';
import EventDetailedInfo from './EventDetailedInfo';
import { Grid } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import { objectToArray } from '../../../app/common/util/helpers';
import { goingToEvent, cancelGoingToEvent } from '../../user/userActions'


/*
	withFirestore HOC ile component e firebase ve firestore propsları gelio ve içinde her şey var
*/
const mapState = (state) => {
	let event = {};

	if (state.firestore.ordered.events && state.firestore.ordered.events[0]) {
		// filter metodu array dönüyor bize ilk bulduu lazım
		event = state.firestore.ordered.events[0];
	}

	return { 
		event,
		auth: state.firebase.auth,

	 };
};

const actions = {
	goingToEvent,
	cancelGoingToEvent
}

class EventDetailedPage extends Component {
	async componentDidMount() {
		const { firestore, match } = this.props;
		await firestore.setListener(`events/${match.params.id}`);
		//let event = await firestore.get(`events/${match.params.id}`);
		// if (!event.exists) {
		// 	history.push('/events');
		// 	toastr.error('Sorry', 'Event not found');
		// }
	}
	// bunu yapmassak mesela form submit listener ı gibi listener lar acik kalıo performans sorunu
	async componentWillUnmount() {
		const { firestore, match } = this.props;
		await firestore.unsetListener(`events/${match.params.id}`);
	}

	render() {
		const { event, auth, goingToEvent, cancelGoingToEvent } = this.props;
		const attendees = event && event.attendees && objectToArray(event.attendees);
		const isHost = event.hostUid === auth.uid;
		const isGoing = attendees && attendees.some(a => a.id === auth.uid)
		return (
			<Grid>
				<Grid.Column width={10}>
					<EventDetailedHeader event={event} isHost={isHost} isGoing={isGoing} goingToEvent={goingToEvent} cancelGoingToEvent={cancelGoingToEvent} />
					<EventDetailedInfo event={event} />
					<EventDetailedChat />
				</Grid.Column>
				<Grid.Column width={6}>
					<EventDetailedSideBar attendees={attendees} />
				</Grid.Column>
			</Grid>
		);
	}
}

export default withFirestore(connect(mapState, actions)(EventDetailedPage));
