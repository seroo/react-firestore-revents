/*global google*/

import React, { Component } from 'react';
import { Form, Segment, Button, Grid, Header } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withFirestore } from 'react-redux-firebase';
import { reduxForm, Field } from 'redux-form';
import { createEvent, updateEvent, cancelToggle } from '../eventActions';
import TextInput from '../../../app/common/form/TextInput';
import TextArea from '../../../app/common/form/TextArea';
import SelectInput from '../../../app/common/form/SelectInput';
import DateInput from '../../../app/common/form/DateInput';
import PlaceInput from '../../../app/common/form/PlaceInput';
import { composeValidators, combineValidators, isRequired, hasLengthGreaterThan } from 'revalidate';
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import  Script  from 'react-load-script';

const mapState = (state, ownProps) => {

	let event = {};

	if (state.firestore.ordered.events && state.firestore.ordered.events[0]) {
		// filter array dönüyor o yüzden 0. index
		event = state.firestore.ordered.events[0];
	}
	return {
		// buraları redux hallediyo ve manage butonu sonrası tum verileri alio
		initialValues: event,
		event: event /* kısace sadece event de yazilabilirdi */
	};
};
// bu actionslar propa ekleniyo aslinde mapDispatchToProps metodu bu sanki
const actions = {
	createEvent,
	updateEvent,
	cancelToggle,
};

// SelectInput icin categoriler
const category = [
	{ key: 'drinks', text: 'Drinks', value: 'drinks' },
	{ key: 'culture', text: 'Culture', value: 'culture' },
	{ key: 'film', text: 'Film', value: 'film' },
	{ key: 'food', text: 'Food', value: 'food' },
	{ key: 'music', text: 'Music', value: 'music' },
	{ key: 'travel', text: 'Travel', value: 'travel' }
];

const validate = combineValidators({
	title: isRequired({ message: 'The event title is required' }),
	category: isRequired({ message: 'Please provide a category' }),
	description: composeValidators(
		isRequired({ message: 'Please enter a description' }),
		hasLengthGreaterThan(4)({ message: 'Description needs to be at least 5 characters' })
	)(),
	city: isRequired('city'),
	venue: isRequired('venue'),
	date: isRequired('date')
});

class EventForm extends Component {
	state = {
		cityLatLng: {},
		venueLatLng: {},
		scriptLoaded: false
	};

	async componentDidMount() {
		const {firestore, match} = this.props;
		await firestore.setListener(`events/${match.params.id}`)
	
	}

	async componentWillUnmount() {
		const {firestore, match} = this.props;
		await firestore.unsetListener(`events/${match.params.id}`)
	}

	handleScriptLoaded = () => this.setState({ scriptLoaded: true });

	handleCitySelect = (selectedCity) => {
		geocodeByAddress(selectedCity)
		.then(results => getLatLng(results[0]))
		.then(latlng => {
			this.setState({
				cityLatLng: latlng
			});
		})
		.then(() => { /* mouse ile autocomplete sonuca tiklanmiyordu */
			this.props.change("city", selectedCity)
		})
	};
	handleVenueSelect = (selectedVenue) => {
		geocodeByAddress(selectedVenue)
		.then(results => getLatLng(results[0]))
		.then(latlng => {
			this.setState({
				venueLatLng: latlng
			});
		})
		.then(() => { /* mouse ile autocomplete sonuca tiklanmiyordu */
			this.props.change("city", selectedVenue)
		})
	};

	onFormSubmit = (values) => {
		values.venueLatLng = this.state.venueLatLng;
		if (this.props.initialValues.id) {
			if (Object.keys(values.venueLatLng).length === 0) {
				values.venueLatLng = this.props.event.venueLatLng
			}
			this.props.updateEvent(values);
			this.props.history.goBack();
		} else {
			
			this.props.createEvent(values);
			this.props.history.push('/events');
		}
	};

	render() {
		const { invalid, submitting, pristine, event, cancelToggle } = this.props;

		return (
			<Grid>
				<Script
					url="https://maps.googleapis.com/maps/api/js?key=AIzaSyABYSFLCZPCJjMbA8S4PYVdZVKwISuhQXI&libraries=places"
					onLoad={this.handleScriptLoaded}
				/>
				<Grid.Column width={10}>
					<Segment>
						<Header sub color="teal" content="Event Details" />
						{/*handleSubmit redux ile gelen bir metod*/}
						<Form onSubmit={this.props.handleSubmit(this.onFormSubmit)}>
							<Field
								name="title"
								type="text"
								component={TextInput}
								placeholder="Give Your Event a Name"
							/>
							<Field
								name="category"
								type="text"
								component={SelectInput}
								options={category}
								multiple={false /*true olursa coktan secmeli*/}
								placeholder="What is your event about"
							/>
							<Field
								name="description"
								type="text"
								row={3}
								component={TextArea}
								placeholder="Tell us about your event"
							/>
							<Header sub color="teal" content="Event Location Details" />
							<Field
								name="city"
								type="text"
								component={PlaceInput}
								options={{
									types: [ '(cities)' ]
								}}
								placeholder="Event City"
								onSelect={this.handleCitySelect}
							/>
							{this.state.scriptLoaded && 
							<Field
								name="venue"
								type="text"
								component={PlaceInput}
								options={{
									location: new google.maps.LatLng(this.state.cityLatLng),
									radius: 30000,
									 types: [ 'establishment' ] 
									}}
								placeholder="Event Venue"
								onSelect={this.handleVenueSelect}
							/>}
							<Field
								name="date"
								type="text"
								component={DateInput}
								dateFormat="YYYY-MM-DD HH:mm"
								timeFormat="HH:mm"
								showTimeSelect
								placeholder="Date and Time of event"
							/>
							<Button disabled={invalid || submitting || pristine} positive type="submit">
								Submit
							</Button>
							<Button onClick={this.props.history.goBack} type="button">
								Cancel
							</Button>
							<Button 
							onClick={() => cancelToggle(!event.cancelled, event.id)}
							type='button'
							color={event.cancelled ? 'green' : 'red'}
							floated='right'
							content={event.cancelled ? 'Reactivate Event' : 'Cancel the Event'}
							/>
						</Form>
					</Segment>
				</Grid.Column>
			</Grid>
		);
	}
}

export default withFirestore(connect(mapState, actions)(
	reduxForm({
		form: 'eventForm',
		enableReinitialize: true /*props degisirse yeniden baslatsin yukaridaki create butonu calismiodu*/,
		validate
	})(EventForm)));
