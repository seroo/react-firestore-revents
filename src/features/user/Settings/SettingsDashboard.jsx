import React from 'react'
import { Grid } from 'semantic-ui-react'
import SettingsNav from './SettingsNav';
import { Switch, Route, Redirect } from 'react-router-dom';
import BasicPage from './BasicPage';
import { updateProfile } from '../userActions';
import AboutPage from './AboutPage';
import PhotosPage from './PhotosPage';
import AccountPage from './AccountPage';
import { connect } from 'react-redux';
import { updatePassword } from '../../auth/authActions';

const actions = {
  updatePassword,
  updateProfile
}

const mapState = (state) => ({
  // ilki index js teki render() i firebaseIsREady ile wraplemeden onceki hali
  // providerId: state.firebase.auth.isLoaded && state.firebase.auth.providerData[0].providerId
  providerId: state.firebase.auth.providerData[0].providerId,
  user: state.firebase.profile
})


const SettingsDashboard = ({updatePassword, providerId, user, updateProfile}) => {
  return (
    <Grid>
      <Grid.Column width={12}>
      <Switch>
        <Redirect exact from='/settings' to='/settings/basic'/>
        <Route path='/settings/basic' render={() => <BasicPage updateProfile={updateProfile} initialValues={user} />  } />
        <Route path='/settings/about' render={() => <AboutPage updateProfile={updateProfile} initialValues={user} />  } />
        <Route path='/settings/photos' component={PhotosPage} />
        {/* render ile yapmamızın sebebi props gecmek zorunda kalmak */}
        <Route path='/settings/account' render={() => <AccountPage updatePassword={updatePassword} providerId={providerId} /> } />
      </Switch>
      </Grid.Column>
      <Grid.Column width={4}>
      <SettingsNav />
      </Grid.Column>
    </Grid>
  )
}

export default connect(mapState, actions)( SettingsDashboard)
