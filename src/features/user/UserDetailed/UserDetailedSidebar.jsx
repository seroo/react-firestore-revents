import React from 'react';
import { Button, Segment } from 'semantic-ui-react';
const UserDetailedSidebar = () => {
	return (
		<div>
			<Segment>
				<Button color="teal" fluid basic content="Edit Profile" />
			</Segment>
		</div>
	);
};

export default UserDetailedSidebar;
