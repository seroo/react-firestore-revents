import React from 'react';
import { Segment, Header, Image } from 'semantic-ui-react';


const UserDetailedPhotos = ({photos}) => {
    console.log(photos);
    
	return (
		<div>
			<Segment attached>
				<Header icon="image" content="Photos" />

				<Image.Group size="small">
					{photos && photos.length > 0 && photos.map(photo => <Image src={photo.url} />) }

					
				</Image.Group>
			</Segment>
		</div>
	);
};

export default UserDetailedPhotos;
