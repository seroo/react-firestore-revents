import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import App from './app/layout/App';
import ReduxToastr from 'react-redux-toastr';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { configureStore } from './app/store/configureStore';
import ScrollToTop from './app/common/util/ScrollToTop';
//import { loadEvents } from './features/event/eventActions'

const store = configureStore();
//store.dispatch(loadEvents());

const rootElement = document.getElementById('root');

let render = () => {
	ReactDOM.render(
		<Provider store={store}>
			<BrowserRouter>
				<ScrollToTop>
					<ReduxToastr position="bottom-right" transitionIn="fadeIn" transitionOut="fadeOut" />
					<App />
				</ScrollToTop>
			</BrowserRouter>
		</Provider>,
		rootElement
	);
};

if (module.hot) {
	module.hot.accept('./app/layout/App', () => {
		setTimeout(render);
	});
}

// firebase ready olmadan renderlama -- bu da bazi componentlerin veriden once yuklenmesini engelliyo
store.firebaseAuthIsReady.then(() => {
	render();
});

registerServiceWorker();
